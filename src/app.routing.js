import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import { Dashboard } from './components/dashboard/dashboard.component';

//auth components
import { Login } from './components/auth/login/login';
import { Register } from './components/auth/register/register';

//user components
import { Update } from './components/user/userUpdate.component';
import { userList } from './components/user/list.user.component';
import { sellerList } from './components/user/list.seller.component';

//header and sidebar
import Header from './components/header/headerRouter';
import Sidebar from './components/sidebar/sidebar.component';

//product components
import { AddProduct } from './components/product/product-add';
import { userProfile } from './components/user/userProfile.component';
import { ForgotPassword } from './components/password-reset/forgot-password.component';
import { ResetPassword } from './components/password-reset/reset-password.component';
import { Homepage } from './components/homepage/homepage.component';


class Notfound extends React.Component {
    render() {
        return (
            <div>
                <h3>Page not found</h3>
                <p>Error: 404</p>
            </div>
        )
    }
}

const PageRoute = ({ component: Component, ...data }) => {

    return (
        <Route {...data} render={(props) => {
            return (
                <>
                    <div className="whole-content">
                        <Component {...props} />
                    </div>
                </>
            )

        }} />
    )
}

const ProtectedPageRoute = ({ component: Component, ...data }) => {
    const adminonly = ['userlist', 'sellerlist'];
    const urlpath = data.path.split('/');
    let ok = true;
    adminonly.forEach(e => {
        urlpath.forEach(e2 => {
            if (e === e2) {
                if (localStorage.getItem('role') !== 'admin') {
                    ok = false
                }
            }
        })
    });
    if (ok) {
        return (
            <Route {...data} render={(props) => {
                return (
                    localStorage.getItem('token')
                        ? (
                            <div>
                                <div className="header">
                                    <Header {...props} />
                                </div>
                                <div className="sidebar">
                                    <Sidebar />
                                </div>
                                <div className="mid-content">
                                    <Component {...props} />
                                </div>
                            </div>
                        )
                        : (
                            <Redirect to="/"></Redirect>
                        )
                )
            }} />
        )
    } else {
        return (
            <Route {...data} render={(props) => {
                return (
                    <Redirect to="/dashboard"></Redirect>
                )
            }} />
        )
    }

}

let ComponentRouter = () => {
    return (
        <Router>
            <Switch>
                {/* Auth */}
                <PageRoute exact path="/" component={Homepage}></PageRoute>
                <PageRoute exact path="/register" component={Register}></PageRoute>
                <PageRoute exact path="/login" component={Login}></PageRoute>
                <PageRoute exact path="/forgot-password" component={ForgotPassword}></PageRoute>
                <PageRoute exact path="/reset-password/:resetToken" component={ResetPassword}></PageRoute>
                {/* Dashboard */}
                <ProtectedPageRoute exact path="/dashboard" component={Dashboard}></ProtectedPageRoute>
                {/* User Routes */}
                <ProtectedPageRoute exact path="/user/userlist" component={userList}></ProtectedPageRoute>
                <ProtectedPageRoute exact path="/user/sellerlist" component={sellerList}></ProtectedPageRoute>
                <ProtectedPageRoute exact path="/user/:userName/update" component={Update}></ProtectedPageRoute>
                <ProtectedPageRoute exact path="/user/:userName" component={userProfile}></ProtectedPageRoute>
                {/* Product Routes */}
                <ProtectedPageRoute exact path="/product/add" component={AddProduct}></ProtectedPageRoute>
                <PageRoute component={Notfound}></PageRoute>
            </Switch>
        </Router>
    )
}

export default ComponentRouter;