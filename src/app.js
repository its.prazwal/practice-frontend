import React from 'react';
import AppRouting from './app.routing';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css'

import './app.css';
const App = () => {
    return (
        <div>
            <ToastContainer autoClose={2500} draggable={false} />
            <AppRouting />
        </div>
    )
}

export default App;