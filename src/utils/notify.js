import { toast } from 'react-toastify';

function success(msg) {
    toast.success(msg, {
        position: toast.POSITION.TOP_CENTER
    });
}

function info(msg) {
    toast.info(msg, {
        position: toast.POSITION.BOTTOM_CENTER

    });
}

function warning(msg) {
    toast.warn(msg, {
        position: toast.POSITION.BOTTOM_LEFT
    })
}

function errorHandler(err) {
    let msg = err && err.response && err.response.data ? err.response.data.msg : 'error';
    toast.error(msg);

}

export default {
    success,
    info,
    warning,
    errorHandler
}