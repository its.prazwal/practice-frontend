import Axios from 'axios';

const http = Axios.create({
    baseURL: process.env.REACT_APP_BASE_URL,
    responseType: 'json'
})

const usBodyHeader = {
    'Content-type': 'application/json'
}

let sBodyHeader = {
    'Content-type': 'application/json',
    'Authorization': localStorage.getItem('token')
}
/**
 * http get request
 * @param {string} url 
 * @param {object} headers
 * @param {object} params
 * @param {string} responseType 
 */
function get(url, { params = {}, responseType = 'json' } = {}, secured = false) {
    return http({
        method: "GET",
        url,
        headers: secured ? sBodyHeader : usBodyHeader,
        params,
        responseType
    })
}

function post(url, { body = {}, responseType = 'json' } = {}, secured = false) {
    return http({
        method: "POST",
        url,
        headers: secured ? sBodyHeader : usBodyHeader,
        data: body,
        responseType
    })
}

function put(url, { body = {}, responseType = 'json' } = {}, secured = false) {
    return http({
        method: "PUT",
        url,
        headers: secured ? sBodyHeader : usBodyHeader,
        data: body,
        responseType
    })
}

function remove(url, { params = {}, responseType = 'json' } = {}, secured = false) {
    return http({
        method: "DELETE",
        url,
        headers: secured ? sBodyHeader : usBodyHeader,
        params,
        responseType
    })
}

function upload(method, url, files) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        const formData = new FormData();
        if (files.length) {
            formData.append('img', files[0], files[0].name)
        }
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                resolve(xhr.response);
            }
        }
        xhr.open(method, url, true);
        xhr.send(formData);
    })
}

export default {
    get,
    post,
    put,
    remove,
    upload
}