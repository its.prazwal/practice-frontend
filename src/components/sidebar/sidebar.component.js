import React from "react";
import { Link } from "react-router-dom";

import "./sidebar.component.css";

const Sidebar = () =>{

    return(
        <ul className="side-bar-list">
            <li className="side-bar-item first-item">
                <Link to= "/dashboard">Dashboard</Link>
            </li>
            <li className="side-bar-item">
                <Link to= "/product/add">Add product</Link>
            </li>
            <li className="side-bar-item">
                <Link to= "/product/view">View Product</Link>
            </li>
            <li className="side-bar-item">
                <Link to= "/product/search">Search Product</Link>
            </li>
        </ul>
    )
}

export default Sidebar;