import React from 'react';

import httpClient from '../../utils/httpClient';
import notify from '../../utils/notify';

export class userList extends React.Component {
    constructor() {
        super();
        this.state = {
            isLoading: true,
            userDetail: []
        }
    }

    componentDidMount() {
        httpClient.get('/user/role/user', {}, true)
            .then(data => {
                this.setState({
                    userDetail: data.data,
                    isLoading: false
                })

            })
            .catch(err => notify.errorHandler(err));
    }

    render() {
        let showUsers = this.state.userDetail.map((users, i) => {
            if (users.dateOfBirth) {
                users.dateOfBirth = users.dateOfBirth.split('T')[0];
            }
            return (
                <tr key={i}>
                    <td>{i + 1}</td>
                    <td>{users.firstName} {users.lastName}</td>
                    <td>{users.dateOfBirth}</td>
                    <td>{users.mobileNo}</td>
                    <td>{users.emailId}</td>
                    <td>{users.status}</td>
                    <td>
                        <button type="button" className="btn btn-primary">Disable</button>
                        <button type="button" className="btn btn-danger">Ban</button>
                    </td>
                </tr>
            )
        })

        let pageLoaded = this.state.isLoading
            ? <div className="loader"></div>
            : <>
                <h2>List of users.</h2>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Name</th>
                            <th>Date of Birth</th>
                            <th>Mobile No</th>
                            <th>Email ID</th>
                            <th>status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {showUsers}
                    </tbody>
                </table> </>;

        return (
            <>
                {pageLoaded}
            </>
        )
    }

}