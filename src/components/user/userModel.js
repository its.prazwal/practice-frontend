let userModel = {
    firstName: '',
    lastName: '',
    userName: '',
    dateOfBirth: '',
    mobileNo: '',
    emailId: '',
    gender: '',
    country: '',
    passWord: '',
    confirmPassword: ''
}

let userError = {
    unError: '',
    emailError: '',
    pwError: '',
    cpwError: ''
}

export default {
    userModel,
    userError
}