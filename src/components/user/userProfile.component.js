import React from 'react';
import { Redirect, Link } from 'react-router-dom';
import httpClient from '../../utils/httpClient';
import notify from '../../utils/notify';

export class userProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            userDetail: {},
            urlmatch: true
        }
    }

    componentDidMount() {
        const urlUsername = this.props.match.params['userName'];
        if (urlUsername === localStorage.getItem('user')) {
            httpClient.get(`/user/task/${localStorage.getItem('id')}`, {}, true)
                .then(user => {
                    this.setState({ userDetail: user.data })
                })
                .catch(err => notify.errorHandler(err));
        } else {
            this.setState({ urlmatch: false });
        }

    }

    handleShowPic = (e) => {
        var modal = document.getElementById("myModal");
        var modalImg = document.getElementById("img01");
        modal.style.display = "block";
        modalImg.src = e.target.src;
        var span = document.getElementsByClassName("close")[0];
        span.onclick = function () {
            modal.style.display = "none";
        }

    }

    render() {
        let { userName, dateOfBirth, mobileNo, gender, emailId, firstName, lastName, image } = this.state.userDetail;
        let profilePicUrl = `${process.env.REACT_APP_PROFILE_URL}/userimg/${image}`;
        let updateUrl = `/user/${userName}/update`
        let profilePicture = image
            ? <img id="profile-img" src={profilePicUrl} alt="profile.png" onClick={this.handleShowPic}></img>
            : <img id="profile-img" src={require('./../../profile-pic.png')} alt="profile.png" onClick={this.handleShowPic}></img>
        let showUserInfo = this.state.urlmatch
            ? <table>
                <tr><th>Username: </th><td>{userName}</td></tr>
                <tr><th>Date Of Birth: </th><td>{(dateOfBirth)}</td></tr>
                <tr><th>Mobile Number: </th><td>{mobileNo}</td></tr>
                <tr><th>Gender: </th><td>{gender}</td></tr>
                <tr><th>Email Address: </th><td>{emailId}</td></tr>
            </table>
            : <Redirect to="/dashboard"></Redirect>



        return (
            <div className="profile-container">
                <div id="myModal" className="modal">
                    <span className="close">&times;</span>
                    <img className="modal-content" alt="profile.png" id="img01"></img>
                </div>
                <div className="profile-detail-left">
                    {profilePicture}
                    <Link to={updateUrl}><button>Edit Profile</button></Link>
                    <div className="left-content-box">
                        <h4>. {firstName} . {lastName} .</h4>
                        {showUserInfo}
                    </div>
                </div>
                <div className="profile-detail-right">
                    <div id="right1" className="right-content-box"><h6>Total Products: 10</h6></div>
                    <div id="right2" className="right-content-box"><h6>Products Sold: 5</h6></div>
                    <div id="right3" className="right-content-box"><h6>Products on Sale: 5</h6></div>
                    <div id="right4" className="right-content-box"><h6>Reviews: 7</h6></div>
                </div>
            </div>
        )
    }
}