import React from 'react';

import httpClient from '../../utils/httpClient';
import notify from '../../utils/notify';

export class Update extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {},
            image: null,
            error: {
                emExist: '',
                mnExist: ''
            },
            isloaded: false,
            validForm: false,
            isSubmiting: false
        }
    }

    handleChange = (e) => {
        let { name, value, type } = e.target;
        if (type === 'file') { this.setState({ image: e.target.files }) }
        else {
            this.setState((preSt) => ({
                data: {
                    ...preSt.data,
                    [name]: value
                },
                error: {
                    emExist: '',
                    mnExist: '',
                }
            }), () => this.validateForm(name));
        };
    }

    validateForm = (ename) => {
        if (ename === 'emailId' || ename === 'mobileNo') {
            httpClient.get('/user/register', { params: { [ename]: this.state.data[ename] } })
                .catch(err => {
                    if (err.response.data !== this.state.data._id) {
                        if (ename === 'emailId') {
                            this.setState({
                                error: { emExist: 'Email Address is already in use.' }
                            })
                        }
                        if (ename === 'mobileNo') {
                            this.setState({
                                error: { mnExist: 'Mobile Number is already in use.' }
                            })
                        }
                    }
                });
        }
    }

    componentDidMount() {
        if (this.state.data) {
            this.setState({ isloaded: true });
        }
        httpClient.get(`/user/task/${localStorage.getItem('id')}`, {}, true)
            .then(user => {
                Object.keys(user.data).forEach(keydata => {
                    this.setState((preSt) => ({
                        data: {
                            ...preSt.data,
                            [keydata]: user.data[keydata]
                        },
                        isloaded: true
                    }))
                })
            })
            .catch(err => notify.errorhandler(err));
    }

    submitUpdate = (e) => {
        this.setState({ isSubmiting: true })
        httpClient.put(`/user/task/${this.state.data._id}`, { body: this.state.data }, true)
            .then(data => {
                this.setState({ isSubmiting: false });
                notify.success('Profile Updated');
                this.props.history.push('/dashboard');
            })
            .catch(err => {
                notify.errorHandler(err);
                this.setState({ isSubmiting: false });
            });
        e.preventDefault();
    }

    handlePicChange = (e) => {
        const { name, files } = e.target;
        if (name === 'editProfilePic') {
            document.getElementById('upload-pic').click();
        }
        else if (name === 'image') {
            if (!files[0].type.includes('image')) {
                notify.info('Invalid format.')
            } else {
                httpClient.upload('POST', `${process.env.REACT_APP_BASE_URL}/user/upload/${localStorage.getItem('id')}`, files)
                    .then(data => {
                        notify.success(data);
                        this.props.history.push(`/user/${this.state.data.userName}`)
                    });
            }
        } else {

        }
    }

    handleShowPic = (e) => {
        notify.success('showing pic');
        var modal = document.getElementById("myModal");
        var modalImg = document.getElementById("img01");
        modal.style.display = "block";
        modalImg.src = e.target.src;
        var span = document.getElementsByClassName("close")[0];
        span.onclick = function () {
            modal.style.display = "none";
        }

    }

    clearAll = () => {
        this.setState({
            data: {},
            error: {
                emExist: '',
                mnExist: ''
            },
            validForm: false,
            isSubmiting: false
        })
    }

    render() {
        let button = this.state.isSubmiting
            ? <button className="btn btn-info" type="button" disabled>Updating Info</button>
            : <button className="btn btn-primary" type="submit">Update Info</button>

        let updateForm = <>
            <label> First Name </label>
            <input className="form-control" type="text" placeholder="firstName" defaultValue={this.state.data.firstName} name="firstName" onChange={this.handleChange}></input>
            <label> Last Name</label>
            <input onChange={this.handleChange} className="form-control" type="text" placeholder="lastName" defaultValue={this.state.data.lastName} name="lastName"></input>
            <label> Mobile Number</label>
            <input onChange={this.handleChange} className="form-control" type="number" placeholder="mobileNo" defaultValue={this.state.data.mobileNo} name="mobileNo"></input>
            <p className="danger">{this.state.error.mnExist}</p>
            <label> Date of Birth</label>
            <input onChange={this.handleChange} className="form-control" type="date" placeholder="dataOfBirth" defaultValue={this.state.data.dateOfBirth} name="dateOfBirth"></input>
            <label> Email Address</label>
            <input onChange={this.handleChange} className="form-control" type="text" placeholder="email address" defaultValue={this.state.data.emailId} name="emailId"></input>
            <p className="danger">{this.state.error.emExist}</p>
            <label> Gender</label>
            <input onChange={this.handleChange} className="form-control" type="text" placeholder="gender" defaultValue={this.state.data.gender} name="gender"></input>
            <label>Choose Photo</label>
            <br />
            {button}
            <input className="btn btn-primary" type="reset" onClick={this.clearAll} value="reset"></input>
        </>

        let heading = this.state.isloaded
            ? <h3>Update Your Profile</h3>
            : <h3>Loading Your Profile</h3>

        let profilePicture = this.state.data.image
            ? <img id="myImg" src={`${process.env.REACT_APP_PROFILE_URL}/userimg/${this.state.data.image}`} className="profile-pic" alt="profile.png" onClick={this.handleShowPic}></img>
            : <img id="myImg" src={require('./../../profile-pic.png')} className="profile-pic" alt="profile.png"></img>;

        let loaded = this.state.isloaded
            ? <>
                <div id="myModal" className="modal">
                    <span className="close">&times;</span>
                    <img className="modal-content" alt="profile.png" id="img01"></img>
                </div>
                <div className="profile-pic-container">
                    {profilePicture}
                    <button name="editProfilePic" className="profile-pic-edit" onClick={this.handlePicChange}>Edit</button>
                    <input id="upload-pic" type="file" name="image" onChange={this.handlePicChange} accept='.jpeg,.jpg,.png'></input>
                </div>
                <br />
                <form className="form-group" onSubmit={this.submitUpdate} noValidate>
                    {updateForm}
                </form>
            </>
            : <div className="loader"></div>;

        return (
            <div className="auth-container">
                {heading}
                {loaded}
            </div>
        )
    }
}