import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import httpClient from '../../utils/httpClient';
import notify from '../../utils/notify';

let defaultState = {
    emailId: ''
}

export class ForgotPassword extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultState
            },
            error: {
                ...defaultState
            },
            isSubmitting: false,
            validForm: false
        }
    }

    handleChange = (e) => {
        let { name, value } = e.target;
        this.setState((preSt) => ({
            data: {
                ...preSt.data,
                [name]: value
            }
        }), () => this.validateError(name));
    }

    validateError(ename) {
        let err;
        switch (ename) {
            case 'emailId':
                err = this.state.data.emailId
                    ? this.state.data.emailId.includes('@')
                        ? ''
                        : 'Please enter valid email Address'
                    : 'Email address is required.';
                break;
            default:
                break;
        }
        this.setState((preSt) => ({
            error: {
                ...preSt.error,
                [ename]: err
            }
        }), () => {
            this.validateForm();
        })
    }

    validateForm() {
        const errors = Object.values(this.state.error);
        if (!errors[0]) {
            this.setState({ validForm: true })
        } else {
            this.setState({ validForm: false });
        }
    }

    handleSubmit = (e) => {
        this.setState({ isSubmitting: true });
        httpClient.post('/user/forgot-password', { body: this.state.data })
            .then(data => {
                notify.info('Password reset link send to your email.');
                this.setState({ isSubmitting: false });
                this.props.history.push('/');
            })
            .catch(err => {
                notify.errorHandler(err);
                this.setState({
                    data: { emailId: '' },
                    error: { emailId: '' },
                    isSubmitting: false,
                    validForm: false
                })
            })
        e.preventDefault();
    }

    render() {
        let submitButton = this.state.validForm
            ? !this.state.isSubmitting
                ? <button type="submit" className="btn btn-primary">Submit</button>
                : <button type="submit" className="btn btn-primary" disabled>Submitting</button>
            : <button type="submit" className="btn btn-secondary" disabled>Submit</button>
        return (
            <>
                <h2>Forgot Your password?</h2>
                <p>Please Enter your email address to reset your password.</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label htmlFor="emailId">Email Address</label>
                    <input type="text" className="form-control" name="emailId" onChange={this.handleChange} placeholder="Email Address" value={this.state.data.emailId}></input>
                    <p className="danger">{this.state.error.emailId}</p>
                    <br />
                    {submitButton}
                </form>
                <p>Back to <Link to="/">Login</Link></p>
            </>
        )
    }
}