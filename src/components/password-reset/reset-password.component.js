import React, { Component } from 'react';
import httpClient from './../../utils/httpClient';
import notify from './../../utils/notify';

let defaultState = {
    passWord: '',
    confirmPassword: ''
}

export class ResetPassword extends Component {
    resetToken;
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultState
            },
            error: {
                ...defaultState
            },
            isSubmitting: false,
            validForm: false
        }
    }

    componentDidMount() {
        this.resetToken = this.props.match.params['resetToken'];
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState((preSt) => ({
            data: {
                ...preSt.data,
                [name]: value
            }
        }), () => this.validateError(name));
    }

    validateError(ename) {
        let err;
        switch (ename) {
            case 'passWord':
                err = this.state.data[ename]
                    ? ''
                    : 'Password is a required field.';
                break;
            case 'confirmPassword':
                err = this.state.data.passWord === this.state.data[ename]
                    ? ''
                    : 'Passwords didnot matched.';
                break;
            default:
                break;
        }
        this.setState((preSt) => ({
            error: {
                ...preSt.error,
                [ename]: err
            }
        }), () => this.validateForm());
    }

    validateForm() {
        const errors = Object.values(this.state.error).filter(val => val);
        if (!errors.length) {
            this.setState({ validForm: true });
        } else {
            this.setState({ validForm: false });
        }
    }

    handleSubmit = (e) => {
        this.setState({ isSubmitting: true });
        httpClient.post(`/user/reset-password/${this.resetToken}`, { body: this.state.data })
            .then(data => {
                this.setState({ isSubmitting: false });
                notify.success('Password reset successful. Please Login to continue.');
                this.props.history.push('/');
            })
            .catch(err => {
                this.setState({ isSubmitting: false });
                notify.errorHandler(err);
            })
        e.preventDefault();
    }

    render() {
        let submitButton = this.state.validForm
            ? !this.state.isSubmitting
                ? <button type="submit" className="btn btn-primary">Submit</button>
                : <button type="submit" className="btn btn-primary" disabled>Submitting</button>
            : <button type="submit" className="btn btn-secondary" disabled>Submit</button>
        return (
            <div>
                <h2>Reset Password</h2>
                <p>Please choose your password wisely.</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Password" onChange={this.handleChange} name="passWord"></input>
                    <p className="danger">{this.state.error.passWord}</p>
                    <label>Confirm Password</label>
                    <input type="password" className="form-control" placeholder="Confirm Password" onChange={this.handleChange} name="confirmPassword"></input>
                    <p className="danger">{this.state.error.confirmPassword}</p>
                    {submitButton}
                </form>
            </div>
        )
    }
}