import React from 'react';
import { Link } from 'react-router-dom'

import httpClient from '../../../utils/httpClient';
import notify from './../../../utils/notify';

import './register.css';

const defaultDataState = {
    firstName: null,
    lastName: null,
    userName: null,
    dateOfBirth: null,
    mobileNo: null,
    emailId: null,
    gender: 'male',
    passWord: null,
    confirmPassword: null,
    role: 'user'
}

const defaultErrorState = {
    userName: 'User Name is a required field.',
    emailId: 'Email Address is a required field.',
    mobileNo: 'Mobile Number is a required field.',
    passWord: 'Password is a required field.',
    confirmPassword: 'Please re-write the password to confirm.'
}

export class Register extends React.Component {
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultDataState
            },
            error: {
                ...defaultErrorState,
            },
            validForm: false,
            isSubmitting: false,
            inputError: false,
        };
    }

    handleChange = (e) => {
        const { name } = e.target;
        let { value } = e.target;
        if (name === 'dateOfBirth') { value = value.split('T')[0] }
        this.setState((preSt) => ({
            data: {
                ...preSt.data,
                [name]: value
            }
        }), () => this.validateError(name, false))
    }

    validateError(ename, exist) {
        let valueError;
        switch (ename) {
            case 'userName':
                if (exist) {
                    valueError = 'Username already in use by another user.'
                } else {
                    valueError = this.state.data[ename]
                        ? this.state.data[ename].length <= 12
                            ? null
                            : 'Please use username having less than 12 character.'
                        : 'Username is a required field.'
                }
                if (valueError) { this.errorField(ename, true) }
                else { this.errorField(ename, false) };
                break;
            case 'emailId':
                if (exist) {
                    valueError = 'Email Address already in use by another user.'
                } else {
                    valueError = this.state.data[ename]
                        ? this.state.data[ename].includes('@', '.com')
                            ? null
                            : 'Please enter valid Email Address'
                        : 'Email Address is a required field.'
                }
                if (valueError) { this.errorField(ename, true) }
                else { this.errorField(ename, false) };
                break;
            case 'mobileNo':
                if (exist) {
                    valueError = 'Mobile Number already in use by another user.'
                } else {
                    valueError = this.state.data[ename]
                        ? this.state.data[ename].length === 10
                            ? null
                            : 'Please enter valid Mobile Number'
                        : 'Mobile Number is a required field.'
                }
                if (valueError) { this.errorField(ename, true) }
                else { this.errorField(ename, false) };
                break;
            case 'passWord':
                valueError = this.state.data[ename]
                    ? this.state.data[ename].length >= 8
                        ? null
                        : 'Please enter atleast 8 character password.'
                    : 'Password is a required field.'
                if (valueError) { this.errorField(ename, true) }
                else { this.errorField(ename, false) };
                break;
            case 'confirmPassword':
                valueError = this.state.data[ename]
                    ? this.state.data[ename] === this.state.data.passWord
                        ? null
                        : 'Passwords didnot matched.'
                    : 'Please re-write the password to confirm.'
                if (valueError) { this.errorField(ename, true) }
                else { this.errorField(ename, false) };
                break;
            default:
                break;
        }

        this.setState((preSt) => ({
            error: {
                ...preSt.error,
                [ename]: valueError
            }
        }), () => this.validateForm());
    }

    validateForm() {
        let errors = Object.values(this.state.error).filter(val => val).length;
        if (errors) { this.setState({ inputError: true, validForm: false }) }
        else { this.setState({ inputError: false, validForm: true }) };
    }

    resetState = () => {
        this.setState({
            data: {
                ...defaultDataState
            },
            error: {
                ...defaultErrorState
            },
            validForm: false,
            isSubmitting: false,
            inputError: false
        })
        const errorsA = ['mobileNo', 'emailId', 'userName', 'passWord', 'confirmPassword'];
        errorsA.forEach(ename => this.errorField(ename, false));
    }

    errorField(id, bl) {
        const element = document.getElementById(`reG${id}`);
        if (bl) {
            element.classList.remove('reGnotErrorField');
            element.classList.add("reGerrorField")
        } else {
            element.classList.add('reGnotErrorField');
            element.classList.remove("reGerrorField")
        }

    }

    handleSubmit = (e) => {
        e.preventDefault();
        httpClient.get('/user/register', { params: { mobileNo: this.state.data.mobileNo } })
            .then(x => {
                httpClient.get('/user/register', { params: { userName: this.state.data.userName } })
                    .then(x => {
                        httpClient.get('/user/register', { params: { emailId: this.state.data.emailId } })
                            .then(x => {
                                httpClient.post('/user/register', { body: this.state.data })
                                    .then(x => {
                                        notify.success('Registration Succesfull.');
                                        notify.info('Please Login to continue.!');
                                        this.props.history.push('/login');//redirect to login
                                    })
                                    .catch(err => {
                                        notify.errorHandler(err);
                                    });
                            })
                            .catch(err => { this.validateError('emailId', true); })
                    })
                    .catch(err => { this.validateError('userName', true); });
            })
            .catch(err => { this.validateError('mobileNo', true) });
    }

    render() {
        var submitButton = this.state.isSubmitting
            ? <button className="reGbutton" type="submit" disabled>Registering</button>
            : <button className="reGbutton" type="submit" disabled={!this.state.validForm}>Register</button>

        var formValidationColor = !this.state.inputError
            ? <div className="reGshowError reGshowError-green">Good to Go.!</div>
            : <div className="reGshowError reGshowError-red">
                {this.state.error.mobileNo || this.state.error.userName || this.state.error.emailId || this.state.error.passWord || this.state.error.confirmPassword}
            </div>

        return (
            <div className="register-box">
                <div className="register-box-top">
                    <h1>REGISTRATION FORM</h1>
                    <p>*please fill out all the required fields.</p>
                </div>
                <form className="register-box-inside" onSubmit={this.handleSubmit}>
                    <div className="accounttype">
                        <input type="radio" value="buyer" id="reGbuyer" name="role" defaultChecked onChange={this.handleChange}></input>
                        <label htmlFor="reGbuyer" className="radio">Buyer</label>
                        <input type="radio" value="seller" id="reGseller" name="role" onChange={this.handleChange}></input>
                        <label htmlFor="reGseller" className="radio">Seller</label>
                    </div>
                    <label className="reGicon" htmlFor="firstName"><i className="fas fa-user-tie"></i></label>
                    <input id="reGfirstName" onChange={this.handleChange} type="text" placeholder="First Name" name="firstName" className="inputField reGnotErrorField"></input>
                    <label className="reGicon" htmlFor="lastName"><i className="fas fa-user-tie"></i></label>
                    <input id="reGlastName" onChange={this.handleChange} type="text" placeholder="Last Name" name="lastName" className="inputField reGnotErrorField"></input>
                    <br />
                    <label className="reGicon" htmlFor="dateOfBirth"><i className="fa fa-calendar"></i></label>
                    <input id="reGdateOfBirth" onChange={this.handleChange} type="date" placeholder="Date Of Birth" name="dateOfBirth" className="inputField reGnotErrorField"></input>
                    <label className="reGicon" htmlFor="mobileNo"><i className="fa fa-mobile"></i></label>
                    <input id="reGmobileNo" onChange={this.handleChange} type="number" placeholder="Mobile Number" name="mobileNo" className="inputField reGnotErrorField"></input>
                    <br />
                    <label className="reGicon" htmlFor="userName"><i className="fas fa-user"></i></label>
                    <input id="reGuserName" onChange={this.handleChange} type="text" placeholder="Username" name="userName" className="inputField reGnotErrorField"></input>
                    <br />
                    <label className="reGicon" htmlFor="emailId"><i className="fa fa-envelope"></i></label>
                    <input id="reGemailId" onChange={this.handleChange} type="text" placeholder="Email Address" name="emailId" className="inputField reGnotErrorField"></input>
                    <br />
                    <div className="gender">
                        <input type="radio" value="male" id="reGmale" name="gender" defaultChecked onChange={this.handleChange}></input>
                        <label htmlFor="reGmale" className="radio">Male</label>
                        <input type="radio" value="female" id="reGfemale" name="gender" onChange={this.handleChange}></input>
                        <label htmlFor="reGfemale" className="radio">Female</label>
                    </div>
                    <br />
                    <label className="reGicon" htmlFor="passWord"><i className="fas fa-user-shield"></i></label>
                    <input id="reGpassWord" onChange={this.handleChange} type="password" placeholder="password" name="passWord" className="inputField reGnotErrorField"></input>
                    <label className="reGicon" htmlFor="confirmPassword"><i className="fas fa-user-shield"></i></label>
                    <input id="reGconfirmPassword" onChange={this.handleChange} type="password" placeholder="confirm password" name="confirmPassword" className="inputField reGnotErrorField"></input>
                    <br />
                    {formValidationColor}

                    <div className="button-div">
                        {submitButton}
                        <input className="button" type="reset" onClick={this.resetState}></input>
                    </div>
                </form>
                <p>Have an Account? <Link to="/login">Login</Link></p>
            </div>
        )
    }
}