import React from 'react';
import { Link } from 'react-router-dom';

import httpClient from '../../../utils/httpClient';
import notify from './../../../utils/notify';

import './login.css'

const defaultState = {
    userName: '',
    passWord: '',
    remember: false
}

export class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                ...defaultState,
            },
            error: {
                ...defaultState
            },
            isSubmitting: false,
            validForm: false
        }
    }

    handleChange = e => {
        const { name, type, checked } = e.target;
        let { value } = e.target;
        if (type === 'checkbox') {
            value = checked
        }
        this.setState((preSt) => ({
            data: {
                ...preSt.data,
                [name]: value
            }
        }), () => this.validateError(name));
    }

    validateError(ename) {
        let valueError;
        switch (ename) {
            case 'userName':
                valueError = this.state.data[ename]
                    ? null
                    : 'Username is a required field.'
                if (valueError) { this.showErrorInput(ename, true) }
                else (this.showErrorInput(ename, false))
                break;
            case 'passWord':
                valueError = this.state.data[ename]
                    ? null
                    : 'Password is a required field.'
                if (valueError) { this.showErrorInput(ename, true) }
                else (this.showErrorInput(ename, false))
                break;
            default:
                break;
        }
        this.setState((preSt) => ({
            error: {
                ...preSt.error,
                [ename]: valueError
            }
        }), () => this.validateForm())
    }

    validateForm() {
        const errors = Object.values(this.state.error).filter(val => val).length
        if (errors) { this.setState({ validForm: false }) }
        else { this.setState({ validForm: true }) }
    }

    showErrorInput(id, bl) {
        const element = document.getElementById(`loG${id}`)
        if (bl) {
            element.classList.add('loGerrorField')
            element.classList.remove('loGnotErrorField')
        } else {
            element.classList.remove('loGerrorField')
            element.classList.add('loGnotErrorField')
        }
    }

    handleSubmit = (e) => {
        httpClient.post('/user/login', { body: this.state.data })
            .then(data => {
                const userDet = data.data
                localStorage.setItem('token', userDet.token);
                localStorage.setItem('id', userDet.id);
                localStorage.setItem('user', userDet.user);
                localStorage.setItem('role', userDet.role);
                localStorage.setItem('remember', this.state.data.remember);
                notify.success(`welcome ${userDet.user}`);
                this.props.history.push('/dashboard');
            })
            .catch(err => {
                notify.errorHandler(err);
                this.showErrorInput('userName', true);
                this.showErrorInput('passWord', true);
            })
        e.preventDefault();
    }

    redirectToReg = () => {
        this.props.history.push('/register')
    }

    componentDidMount() {
        this.setState((preSt) => ({
            data: {
                ...preSt.data,
                userName: localStorage.getItem('user') ? localStorage.getItem('user') : '',
            }
        }))
    }

    showPassword = (e) => {
        const passwordbox = document.getElementById('loGpassWord');
        const passwordToggleIcon = document.getElementById('password-toggle-icon');
        if (passwordbox.type === 'text') {
            passwordbox.setAttribute('type', 'password');
            passwordToggleIcon.classList.remove('fa-eye');
            passwordToggleIcon.classList.add('fa-eye-slash');
        }
        else {
            passwordbox.setAttribute('type', 'text');
            passwordToggleIcon.classList.remove('fa-eye-slash');
            passwordToggleIcon.classList.add('fa-eye');
        }
    }

    render() {
        var submitButton = this.state.isSubmitting
            ? <button className="loGbutton" type="submit" disabled>Logging in</button>
            : <button className="loGbutton" type="submit" disabled={!this.state.validForm}>Login</button>

        var formValidationColor = this.state.validForm
            ? <div className="loGshowError loGshowError-green">Good to Go.!</div>
            : <div className="loGshowError loGshowError-red">
                {this.state.error.userName || this.state.error.passWord}
            </div>

        return (
            <div className="login-box">
                <div className="login-box-top">
                    <h1>Login Page</h1>
                    <p>Enter your details to login</p>
                </div>
                <form className="login-box-inside" onSubmit={this.handleSubmit}>
                    <label className="icon" htmlFor="userName"><i className="fas fa-user"></i></label>
                    <input
                        type="text"
                        id="loGuserName"
                        className="loGnotErrorField"
                        value={this.state.data.userName}
                        onChange={this.handleChange}
                        placeholder="username"
                        name="userName">
                    </input>
                    <div>
                        <label className="icon" htmlFor="passWord"><i className="fas fa-user-shield"></i></label>
                        <input
                            type="password"
                            id="loGpassWord"
                            className="loGnotErrorField"
                            onChange={this.handleChange}
                            placeholder="password"
                            name="passWord">
                        </input>
                        <button type="button" id="password-toggle-button" onClick={this.showPassword}><span id="password-toggle-icon" className="fa fa-eye-slash"></span></button>
                    </div>
                    <br />
                    <div className="loGremember">
                        <label htmlFor="loGremember">Remember User</label>
                        <input type="checkbox" id="loGremember" name="remember" onClick={this.handleChange}></input>
                    </div>
                    {formValidationColor}
                    <br />
                    <div className="loGbutton-div">
                        {submitButton}
                        <button className="loGbutton" type="button" onClick={this.redirectToReg}>Register</button>
                    </div>
                </form>
                <p>Forgot your password? </p>
                <Link to="/forgot-password">Reset Password</Link>

            </div>
        )
    }
}