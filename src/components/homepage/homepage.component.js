import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Homepage extends Component{
    render(){
        return(
            <>
                <h1>PZ PRODUCT STORE</h1>
                <p>The page is under development. <br/> Thank you for your patience</p>
                <h5>If you have access to login, please </h5>
                <Link to="/login">Login</Link>
                <h5>If you want to register, please</h5>
                <Link to="/register">Register</Link>
            </>
        )
    }
}