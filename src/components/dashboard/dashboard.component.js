import React from 'react';
import httpClient from '../../utils/httpClient';
import notify from '../../utils/notify';

import './dashboard.component.css';

export class Dashboard extends React.Component {

    constructor() {
        super();
        this.state = {
            userDetail: {},
            isLoading: true
        }
    }

    componentDidMount() {
        const id = localStorage.getItem('id');
        httpClient.get(`/user/task/${id}`, {}, true)
            .then(data => {
                this.setState({
                    userDetail: data.data,  
                    isLoading: false
                })
            })
            .catch(err => notify.errorHandler(err));
    }

    render() {
        let loaded = this.state.isLoading
            ? <div className="loader"></div>
            : <div>
                <p id="dashboard-head">Welcome '{this.state.userDetail.userName}' to Dashboard</p>
                {/* <button type="button" name="status" value="online" onClick={this.statusChange}>online</button>
                <button type="button" name="status" value="offline" onClick={this.statusChange}>offline</button>
                <button type="button" name="status" value="away" onClick={this.statusChange}>away</button>
                <p><Link to="/update">Update info</Link></p>
                <button type="button" name="deleteAccount" onClick={this.deleteAccount}>Delete Account</button>
                <button type="button" name="logout" onClick={this.logOut}>Logout</button> */}
            </div>
        return (
            loaded
        )
    }
}