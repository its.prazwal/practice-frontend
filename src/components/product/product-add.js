import React from 'react';
// import httpclient from './../../utils/httpClient';
// import notify from './../../utils/notify';

export class AddProduct extends React.Component {
    constructor() {
        super();
        this.state = {
            productData: {}
        }
    }

    handleChange = (e) => {
        var { name, value } = e.target;
        console.log('name - ', name);
        console.log('value - ', value);
        e.preventDefault();
    }

    handleSubmit = (e) => {
        e.preventDefault();
    }
    render() {
        return (
            <div>
                <h2>Add Product</h2>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label htmlFor="mainCategory">Main Category</label>
                    <select className="form-control" id="mainCategory" onChange={this.handleChange} name="mainCategory">
                        <option>--</option>
                        <option>Mens</option>
                        <option>Womens</option>
                        <option>kids</option>
                    </select>
                    <label htmlFor="category">Category</label>
                    <select className="form-control" id="category" onChange={this.handleChange} name="category">
                        <option>--</option>
                        <option>Clothing</option>
                        <option>Traditional Clothing</option>
                        <option>Shoes</option>
                        <option>Bags</option>
                    </select>
                    <label htmlFor="subCategory">Sub Category</label>
                    <select className="form-control" id="subCategory" onChange={this.handleChange} name="subCategory">
                        <option>--</option>
                        <option>abc</option>
                        <option>def</option>
                        <option>ghi</option>
                    </select>
                    <label htmlFor="brand">Brand</label>
                    <input type="text" id="brand" className="form-control" placeholder="Brand" name="brand" onChange={this.handleChange}></input>
                    <label htmlFor="name">Product Name</label>
                    <input type="text" id="name" className="form-control" placeholder="Product Name" name="name" onChange={this.handleChange}></input>
                    <label htmlFor="color">Color</label>
                    <input type="text" id="color" className="form-control" placeholder="Color" name="color" onChange={this.handleChange}></input>
                    <label htmlFor="price">Price</label>
                    <input type="text" id="price" className="form-control" placeholder="price" name="Price" onChange={this.handleChange}></input>
                    <label htmlFor="image">image</label>
                    <input type="file" id="image" className="form-control" name="image" onChange={this.handleChange} accept='.jpeg,.jpg,.png'></input>
                    <button type="submit" className="btn btn-primary">Add Product</button>
                </form>
            </div>
        )
    }
}