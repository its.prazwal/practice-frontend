import React, { Component } from 'react';

import httpClient from './../../utils/httpClient';
import notify from './../../utils/notify';

export class UserStatus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: '',
            statusList: ['online', 'offline', 'away']
        }
    }

    componentDidMount() {
        const id = localStorage.getItem('id');
        httpClient.get(`/user/task/${id}`, {}, true)
            .then(user => this.setState({ status: user.data.status }))
            .catch(err => notify.errorHandler(err));
    }

    changeStatus = (e) => {
        const { value: changedStatus } = e.target;
        httpClient.put(`/user/task/${localStorage.getItem('id')}`, { body: { status: changedStatus } }, true)
            .then(data => { this.setState({ status: changedStatus }) })
            .catch(err => notify.errorHandler(err));
    }

    render() {
        let showStatus = '';
        switch (this.state.status) {
            case 'online':
                showStatus = <span className="dot dot-online"></span>
                break;
            case 'offline':
                showStatus = <span className="dot dot-offline"></span>
                break;
            case 'away':
                showStatus = <span className="dot dot-away"></span>
                break;
            default:
                break;
        }
        let statuslist = this.state.statusList.map((status, i) => {
            if (this.state.status !== status) {
                return (
                    <button type="button" value={status} className="userstatus-choose" onClick={this.changeStatus} key={i}>{status}</button>
                )
            }
            return (null);

        })
        return (
            <div className="userstatus">
                <button className="userstatus-btn">{showStatus} {this.state.status}</button>
                <div className="userstatus-drop">
                    {statuslist}
                </div>
            </div>
        )
    }
}