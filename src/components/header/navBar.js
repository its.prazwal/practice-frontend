import React from 'react';
import { Link } from 'react-router-dom';

//CSS
import './header.css'

let navBar = () => {
    const userrole = localStorage.getItem('role');
    if (userrole === 'admin') {
        return (
            <nav className="nav-bar">
                <ul className="nav-bar-list">
                    <li className="nav-bar-item">
                        <Link to="/user/userlist">User List</Link>
                    </li>
                    <li className="nav-bar-item">
                        <Link to="/user/sellerlist">Seller List</Link>
                    </li>
                </ul>
            </nav>
        )
    }else{
        return null;
    }
}

export default navBar;