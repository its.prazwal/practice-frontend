import React from 'react';
import { Link } from 'react-router-dom';
import notify from '../../utils/notify';
import httpClient from '../../utils/httpClient';

export class UserHead extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userInfo: {}
        }
    }

    deleteUser = () => {
        let confirmation = window.confirm('Please confirm to delete your account permanently.');
        if (confirmation) {
            httpClient.remove(`/user/task/${localStorage.getItem('id')}`, {}, true)
                .then(deleted => {
                    notify.warning('Account has been deleted');
                    localStorage.clear();
                    this.props.history.push('/');
                })
        }
    }

    logOutUser = () => {
        let confirmation = window.confirm('Are you sure to log out?');
        if (confirmation) {
            if(localStorage.getItem('remember')==='false'){
                localStorage.clear();
            }else{
                localStorage.removeItem('token');
                localStorage.removeItem('id');
            }
            notify.info('Logged out');
            this.props.history.push('/');
        }
    }

    render() {
        const usersname = localStorage.getItem('user');
        const userProfileLink = `/user/${usersname}`;
        const userUpdateLink = `/user/${usersname}/update`;
        return (
            <div className="dropdown">
                <button className="dropbtn">User</button>
                <div className="dropdown-content">
                    <Link to={userProfileLink}>{usersname}</Link>
                    <Link to={userUpdateLink}>Update Profile</Link>
                    <button type="button" className="userbtn" onClick={this.deleteUser}>Delete Account</button>
                    <button type="button" className="userbtn" onClick={this.logOutUser}>Log Out</button>
                </div>
            </div>
        )
    }

}
