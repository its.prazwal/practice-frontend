import React from 'react';
import NavBar from './navBar'
import { UserHead } from './userHead';
import { UserStatus } from './userStatus.component';

let header = (props) => {
    return (
        <div>
            <h2 id="header-name">Control Panel</h2>
            <NavBar />
            <UserStatus />
            <UserHead {...props} />
        </div>
    )
}

export default header;